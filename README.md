# Currency Calculator

A simple currency calculator that uses https://www.frankfurter.app/ API to retrieve the current currecy conversion values.

## Functionalities

- write amount, select the currencies to convert, the appropriate value is shown in output currency
- when data is loading, the loading indicator is shown
- error handling
- disabled fields while loading
- dynamic html title values depending of the selected currencies

## Created by

Katlin

![calculator](./public/currencycalc1.png)
