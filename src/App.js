// `https://api.frankfurter.app/latest?amount=100&from=EUR&to=USD`

import { useEffect, useState } from "react";

export default function App() {
  const [amount, setAmount] = useState("100");
  const [inputCurrency, setInputCurrency] = useState("EUR");
  const [outputCurrency, setOutputCurrency] = useState("USD");
  const [rate, setRate] = useState(0);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const controller = new AbortController();
    if (inputCurrency === outputCurrency) {
      return setRate(amount);
    }

    const fetchCurrency = setTimeout(async function fetchCurrency() {
      try {
        setError("");
        setIsLoading(true);
        const response = await fetch(
          `https://api.frankfurter.app/latest?amount=${amount}&from=${inputCurrency}&to=${outputCurrency}`,
          { signal: controller.signal }
        );

        const data = await response.json();
        setRate(data.rates[outputCurrency]);
        setIsLoading(false);
      } catch (err) {
        setError(err);
      }
    }, 500);

    //fetchCurrency();

    return function () {
      controller.abort();
      clearTimeout(fetchCurrency);
    };
  }, [amount, inputCurrency, outputCurrency]);

  useEffect(() => {
    document.title = inputCurrency + " -> " + outputCurrency;
  }, [inputCurrency, outputCurrency]);

  return (
    <div>
      <div className="row">
        <h2>Currency Calculator</h2>
      </div>
      <div className="row">
        <input
          type="number"
          value={amount}
          onChange={(e) => setAmount(Number(e.target.value))}
          disabled={isLoading}
        />
        <select
          value={inputCurrency}
          onChange={(e) => setInputCurrency(e.target.value)}
          disabled={isLoading}
        >
          <option value="USD">USD</option>
          <option value="EUR">EUR</option>
          <option value="GBP">GBP</option>
          <option value="SEK">SEK</option>
        </select>
        <select
          value={outputCurrency}
          onChange={(e) => setOutputCurrency(e.target.value)}
          disabled={isLoading}
        >
          <option value="USD">USD</option>
          <option value="EUR">EUR</option>
          <option value="GBP">GBP</option>
          <option value="SEK">SEK</option>
        </select>
      </div>
      <div>
        {!isLoading && !error && (
          <p className="output">
            Result: {rate} {outputCurrency}
          </p>
        )}
        {!error && isLoading && (
          <p className="output">Loading, please wait...</p>
        )}
        {error && <p className="output">{error.message}</p>}
      </div>
    </div>
  );
}
